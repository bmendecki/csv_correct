import csv
import re

#---------------------------------------------------------------
def csv_correct(src, output):
	with open(src, 'rb') as f:
		reader = csv.reader(f, delimiter=';')		
		cols = next(reader)
		r = re.compile('P1_*') # checking quantity of P1_*
		isP1 = filter(r.match,cols)
		lenP1 = len(isP1)

		with open(output, 'a+') as w:		
			writer = csv.writer(w,delimiter=';')
			writer.writerow(cols)
			for row in reader:		
				p1s = row[:lenP1+1] # how many columns P1_*
				zeros = [i for i,x in enumerate(p1s) if x == '0']
				for item in zeros:
					row[int(item) + lenP1] = ' '			
				writer.writerow(row)
#----------------------------------------------------------------

csv_correct('zadanie2.csv','output.csv')